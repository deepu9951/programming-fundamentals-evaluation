
#############Pet Class########################
class Pet:
    def __init__(self,species = '' ,name = ""):
        self.x = ['Dog','Cat','Horse','Hamster', '']
        self.species = species
        if species not in self.x:
            raise Exception("Unknown Species")
        self.name = name

    def __str__(self):
        if self.name != "":
            return "Species of: " + self.species + " named " + self.name
        else:
            return "Species of :" + self.species + " unnamed "

##################################################


#############Dog Class############################
class Dog(Pet):
    def __init__(self,name = "",chases="Cats"):
        self.name = name
        Pet.__init__(self,"Dog",name) 
        self.chases = chases

    def __str__(self):
        if self.name != "":
            return "Species of :" + self.species + ", named " + self.name + " chases " + self.chases
        else:
            return "Species of: " + self.species + ", unnamed, chases " + self.chases
##################################################


#############Cat Class############################
class Cat(Pet):
    def __init__(self,name ="",hates ="Dogs"):
        self.name=name
        Pet.__init__(self,"Cat",name) 
        self.hates = hates


    def __str__(self):
        if self.name != "":
            return "Species of :" + self.species + ", named, " + self.name + " hates " + self.hates
        else:
            return "Species of: " + self.species + ", unnamed, hates " + self.hates
##################################################


new_pet = Pet()
print(new_pet)
new_pet1 = Pet('Horse','Chicku')
print(new_pet1)
new_dog1 = Dog()
print(new_dog1)
new_dog = Dog('Boban','Ruben')
print(new_dog)
new_cat1 = Cat()
print(new_cat1)
new_cat = Cat('Ram','Ravi')
print(new_cat)
ex_pet = Pet('Rabbit','Mithu')
print(ex_pet)